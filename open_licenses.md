[Open Source Licenses]


-okhttp : https://github.com/square/okhttp
Apache License 2.0, Copyright 2019 Square, Inc.

-ExoPlayer : https://github.com/google/ExoPlayer
Apache License 2.0

-glide : https://github.com/bumptech/glide
Apache License 2.0, 2-Clause BSD License

-gson : https://github.com/google/gson
Apache License 2.0, Copyright 2008 Google Inc.

-MaterialProgressBar : https://github.com/zhanghai/MaterialProgressBar
Apache License 2.0, Copyright 2015 Hai Zhang

-EventBus : https://github.com/greenrobot/EventBus
Apache License 2.0

-android-file-chooser : https://github.com/hedzr/android-file-chooser
Apache License 2.0, Copyright 2019 Hedzr Yeh.

-firebase-android-sdk : https://github.com/firebase/firebase-android-sdk
Apache License 2.0, Android Software Development Kit License



---
Apache License 2.0

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
